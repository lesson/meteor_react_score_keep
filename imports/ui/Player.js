import React from 'react'
import { Players } from '/imports/api/players'

export default class Player extends React.Component {
  render () {
    const player = this.props.player

    return (
      <div key={player._id} className='item'>
        <div className='player'>
          <div>
            <h3 className='player__name'>{player.name}</h3>
            <p className='player__stats'>{player.score} point(s).</p>
          </div>

          <div className='player__actions'>
            <button
              className='button button--round '
              onClick={() =>
                Players.update(
                  { _id: player._id },
                  { $set: { score: player.score + 1 } }
                )}
            >
              +1
            </button>
            <button
              className='button button--round'
              onClick={() =>
                Players.update(
                  { _id: player._id },
                  { $set: { score: player.score - 1 } }
                )}
            >
              -1
            </button>
            <button
              className='button button--round'
              onClick={() => Players.remove({ _id: player._id })}
            >
              x
            </button>
          </div>
        </div>

      </div>
    )
  }
}
