import React from 'react'
import Player from '/imports/ui/Player'

export default class PlayerList extends React.Component {
  render () {
    let players = this.props.players

    if (players.length === 0) {
      return <div className='item item__message'> Add your first player </div>
    }
    return (
      <div>
        {
          players.map(
            (player) => {
              return (
                <Player key={player._id} player={player} />
              )
            }
          )
        }
      </div>
    )
  }
}
