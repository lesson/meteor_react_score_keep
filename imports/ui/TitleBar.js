import React from 'react'

export default class TitleBar extends React.Component {
  renderSubtitle () {
    console.log(this.props)

    if (this.props.subtitle) {
      return <h2 className='title-bar__subtitle'>{this.props.subtitle}</h2>
    }
  }
  render () {
    return (
      <div>
        <h1 className='title-bar'>
          <div className='wrapper'>
            <h1>{this.props.title}</h1>
            {this.renderSubtitle()}
          </div>
        </h1>
      </div>
    )
  }
}
