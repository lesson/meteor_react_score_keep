import React from 'react'
import TitleBar from '/imports/ui/TitleBar'
import AddPlayer from '/imports/ui/AddPlayer'
// import Player from '/imports/ui/Player';
import PlayersList from '/imports/ui/PlayerList'

export default class App extends React.Component {
  render () {
    return (
      <div>
        <TitleBar title={this.props.title} subtitle={this.props.subtitle} />
        <div className='wrapper'>
          <PlayersList players={this.props.players} />
          <AddPlayer players={this.props.players} />
        </div>
      </div>
    )
  }
}
