import React from 'react'
import { Players } from './../api/players'

export default class AddPlayer extends React.Component {
  constructor () {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    Players.insert({
      name: e.target.playerName.value,
      score: 0
    })
    e.target.playerName.value = ''
  }

  render () {
    return (
      <div className='item'>
        <form className='form' onSubmit={this.handleSubmit}>
          <input className='form__input' type='text' name='playerName' placeholder='Player name' />
          <button className='button'>Add player</button>
        </form>
      </div>
    )
  }
}
