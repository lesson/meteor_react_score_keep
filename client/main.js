import React from 'react'
import ReactDom from 'react-dom'
import { Meteor } from 'meteor/meteor'
import { Tracker } from 'meteor/tracker'
import App from './../imports/ui/App'

import {Players} from './../imports/api/players'

Meteor.startup(() => {
  Tracker.autorun(() => {
    let title = 'Score Keep'
    let subtitle = 'Created by Jaspreet Singh'
    let players = Players.find().fetch()
    ReactDom.render(<App title={title} subtitle={subtitle} players={players} />, document.getElementById('app'))
  })
})
